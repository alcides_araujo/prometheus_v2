***
ETL
***

Dentro do nosso código, temos um pacote chamado ``data_ingestion``, onde fica o módulo ``etl.py`` (prometheus/src/data_ingestion/etl.py) que é responsável por organizar o transporte e tratamento dos dados que vão ser utilizados como insumo pelo modelo de clusterização.

Nele, está definido o seguinte processo:
   #. Extração de dados (leitura do BigQuery);
   #. Tratamento preliminar dos dados;
   #. Disponibilização destes dados em formato de dataframe (pandas).

Além do módulo ``etl.py``, este pacote contém outros módulos auxiliares, que existem apenas por questões de organização e legibilidade.

Ao final do processo de ETL, temos como resultado um dataframe com as seguintes características:
    - Cada linha equivale ao perfil de saúde de uma empresa ou plano ou operadora.
    - Cada coluna equivale a um indicador de saúde, ou a uma flag que especifica a qual tipo de cliente / colaborador aquele indicador se refere.
