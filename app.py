import flask
import logging
from datetime import datetime
from src.config.config import ConfigDev, ConfigQa, ConfigPrd
#from src.config.config import ConfigPrd

import sys
import os
from src import train
from src import predict

# Configurar logging.
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s --> %(levelname)s:  %(message)s',
    datefmt='%d/%m/%y %H:%M:%S'
)

# Inicializar aplicação Flask e apontar caminho onde a documentação é gerada.
app = flask.Flask(__name__, static_url_path='/', static_folder='docs/build/html/')


@app.route('/', methods=['GET'])
def serve_sphinx_docs(path='index.html'):
    return app.send_static_file(path)


# @app.route('/', methods=['POST'])
# def start():
#     ConfigPrd() if ('prd-prometheus' in flask.request.url) else ConfigDev()

#     # Eu sei que fazer import dentro do código é feio, mas é a única forma de eu configurar
#     # o runtime environment antes de continuar com o resto do código.
#     #from src import train
#     train.run()

#     logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
#     #logging.debug("teste")
#     return '', 204


@app.route('/train-prometheus/', methods=['POST'])
def train_prometheus():
    
    request_json = flask.request.get_json(force=True)
    
    if request_json['type'] == 'dev':
        ConfigDev()
        train.run(request_json)
        logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
        return "modelos treinados com sucesso e armazenados em dev"

    elif request_json['type'] == 'qa':
        ConfigQa()
        train.run(request_json)
        logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
        return "modelos treinados com sucesso e armazenados em qa"
    
    elif request_json['type'] == 'prod':
        ConfigPrd()
        train.run(request_json)
        logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
        return "modelos treinados com sucesso e armazenados em prod"
    else:
        return "Por favor indicar 'dev', 'qa' ou 'prod' na requisicao do type"

@app.route('/predict-prometheus/', methods=['POST'])
def predict_prometheus():
    
    request_json = flask.request.get_json(force=True)
    
    if request_json['type'] == 'dev':
        ConfigDev()
        predict.run(request_json)
        logging.debug(f"Pipeline de predict finalizado em {datetime.now()}.")
        return "clusters armazenados em dev"

    elif request_json['type'] == 'qa':
        ConfigQa()
        predict.run(request_json)
        logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
        return "clusters armazenados em prod"
    
    elif request_json['type'] == 'prod':
        ConfigPrd()
        predict.run(request_json)
        logging.debug(f"Pipeline de treino finalizado em {datetime.now()}.")
        return "clusters armazenados em prod"
    else:
        return "Por favor indicar 'dev' ou 'prod' na requisicao do type"


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8000)))