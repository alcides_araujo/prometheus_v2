from src.data_ingestion.features import *

from src.config.config import ConfigDev, ConfigQa, ConfigPrd
#from src.config.config import ConfigPrd

#print(ConfigPrd(), os.getenv('GCLOUD_PROJECT'))

def run(request_json):
    """
    Executa o processo de ETL, desde a ingestão de dados do BigQuery \
    até a disponibilização dos dados em formato de dataframe (pandas).

    Returns
    -------
    perfis:
        Sequence[pandas.DataFrame]
    """

    logging.debug("Iniciando pipeline de ETL.")

    df_perfil_por_empresa = get_perfil_por_empresa(request_json)
    df_perfil_por_plano = get_perfil_por_plano(request_json)
    df_perfil_por_operadora = get_perfil_por_operadora(request_json)

    logging.debug("Pipeline de ETL finalizado.")

    return df_perfil_por_empresa, df_perfil_por_plano, df_perfil_por_operadora
