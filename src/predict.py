from google.cloud import bigquery, storage
from datetime import datetime

import pandas as pd
from src.data_ingestion import etl
from src.data_ingestion.features import *
from src.data_ingestion.utils import Quantizer, df_to_bigquery

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler

from src.config.config import ConfigDev, ConfigQa, ConfigPrd
from src.data_ingestion.gcp_utils import download_blob

import re
import sys
import os
from joblib import load

def plot_clusters(X, labels, grupo):
    # Criação de um plot 3D dos clusters.
    fig, ax = plt.figure(figsize=(10, 8)), plt.axes(projection='3d')

    x, y, z = X[:, 0], X[:, 1], X[:, 2]

    ax.scatter3D(x, y, z, c=labels)
    plt.title(f'Clusters - {grupo}')

    # Salvar imagens dos clusters para serem displayados na documentação.
    plt.savefig(f'{os.environ["ROOT_DIR"]}/docs/source/img/clusters_{grupo}.png')


def prediction_pipeline(df: pd.DataFrame, grupo: str = 'empresas') -> pd.DataFrame:
    # Limpeza preliminar do dataframe.
    X = df.fillna(0).drop(
        columns=[
            'idEmpresa', 'dtCompetencia', 'flag_indicador_empresa',
            'IdApoliceSubFaturaPlano', 'dtCompetenciaContapaga', 'flag_indicador_plano',
            'IdOperadora', 'flag_indicador_operadora'
        ],
        errors='ignore'
    )
    
    # carregar modelos
    preprocessing_pipe_path = f'{os.environ["ROOT_DIR"]}/src/models/preprocessing_pipe_{grupo}.joblib'
    cluster_path = f'{os.environ["ROOT_DIR"]}/src/models/cluster_{grupo}.joblib'
    
    preprocessing_pipe = load(preprocessing_pipe_path) 
    clustering_pipe = load(cluster_path)

    # Pré-processamento + Predict.
    preprocessed_X = preprocessing_pipe.transform(X)
    clusters = clustering_pipe.predict(preprocessed_X)

    # Salvar plot dos clusters.
    plot_clusters(preprocessed_X, clusters, grupo)

    # Adicionar labels ao dataframe original.
    output_df = df.copy()
    output_df['cluster'] = clusters
    
    return output_df


def round_(x):
    try:
        return round(x, 3)
    except:
        return x


def run(request_json):
    logging.debug("Iniciando pipeline de treino.")

    # ETL.
    df_perfil_por_empresa, df_perfil_por_plano, df_perfil_por_operadora = etl.run(request_json)
    
    # Baixar modelos treinados
    logging.debug("Baixando modelos treinados")
    
    storage_client = storage.Client.from_service_account_json(json_credentials_path=os.environ['GCLOUD_CREDENTIALS_FILEPATH'])
    bucket = storage_client.get_bucket(os.environ['GCLOUD_BUCKET'])
    blobs = bucket.list_blobs(prefix = os.environ["CLUSTER_FOLDER_NAME"])
    blobs_names = [blob.name for blob in blobs]
    blobs_names.pop(0)
    LOCAL_MODEL_FILEPATH = f'{os.environ["ROOT_DIR"]}/src/models'
    
    for blob in blobs_names:
        
        file_name = re.sub(f'{os.environ["CLUSTER_FOLDER_NAME"]}/', '', blob)
        destination_file = f'{LOCAL_MODEL_FILEPATH}/{file_name}'
        print(destination_file)
        
        download_blob(
            bucket_name=os.environ['GCLOUD_BUCKET'],
            source_blob_name=blob,
            destination_file_name=destination_file
        )
        
    # Predict.
    logging.debug("Gerar os predicts")
    
    benchmark_empresas = prediction_pipeline(df_perfil_por_empresa)
    benchmark_planos = prediction_pipeline(df_perfil_por_plano, grupo='planos')
    benchmark_operadoras = prediction_pipeline(df_perfil_por_operadora, grupo='operadoras')
    
    # Montar resultado.
    benchmark_df = pd.concat([benchmark_empresas, benchmark_planos, benchmark_operadoras])
    benchmark_df['timestamp'] = pd.to_datetime(datetime.now())
    benchmark_df = benchmark_df.applymap(lambda x: round_(x))
    

    # Salvar resultado no BigQuery.
    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("idEmpresa", bigquery.enums.SqlTypeNames.INTEGER),
            bigquery.SchemaField("IdOperadora", bigquery.enums.SqlTypeNames.INTEGER),
            bigquery.SchemaField("IdApoliceSubFaturaPlano", bigquery.enums.SqlTypeNames.INTEGER)
        ]
    )
    
    df_to_bigquery(
        df=benchmark_df,
        table_id=f"{os.environ['GCLOUD_PROJECT']}.fenix_clean.benchmark",
        credentials_json=os.environ['GCLOUD_CREDENTIALS_FILEPATH'],
        job_config=job_config
    )
