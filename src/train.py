from google.cloud import bigquery
from datetime import datetime

import pandas as pd
from src.data_ingestion import etl
from src.data_ingestion.features import *
from src.data_ingestion.utils import Quantizer, df_to_bigquery

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures, MinMaxScaler

from src.config.config import ConfigDev, ConfigQa, ConfigPrd
from src.data_ingestion.gcp_utils import upload_file

import sys
import os
from joblib import dump

def plot_clusters(X, labels, grupo):
    # Criação de um plot 3D dos clusters.
    fig, ax = plt.figure(figsize=(10, 8)), plt.axes(projection='3d')

    x, y, z = X[:, 0], X[:, 1], X[:, 2]

    ax.scatter3D(x, y, z, c=labels)
    plt.title(f'Clusters - {grupo}')

    # Salvar imagens dos clusters para serem displayados na documentação.
    plt.savefig(f'{os.environ["ROOT_DIR"]}/docs/source/img/clusters_{grupo}.png')


def training_pipeline(df: pd.DataFrame, grupo: str = 'empresas') -> pd.DataFrame:
    # Limpeza preliminar do dataframe.
    X = df.fillna(0).drop(
        columns=[
            'idEmpresa', 'dtCompetencia', 'flag_indicador_empresa',
            'IdApoliceSubFaturaPlano', 'dtCompetenciaContapaga', 'flag_indicador_plano',
            'IdOperadora', 'flag_indicador_operadora'
        ],
        errors='ignore'
    )

    # Inicialização do pipeline de pré-processamento.
    preprocessing_pipe = Pipeline([
        ("feature_engineering", PolynomialFeatures(degree=50, include_bias=False, interaction_only=True)),
        ("scale_0_1", MinMaxScaler()),
        ("discretize", Quantizer()),
        ("pca", PCA(n_components=3, random_state=42))
    ])

    # Inicialização do pipeline de clusterização.
    clustering_pipe = Pipeline([
        ("cluster", KMeans(n_clusters=5, n_init=50, max_iter=500, random_state=42))
    ])

    # Pré-processamento + Treino.
    preprocessed_X = preprocessing_pipe.fit_transform(X)
    clusters = clustering_pipe.fit_predict(preprocessed_X)

    # Salvar plot dos clusters.
    plot_clusters(preprocessed_X, clusters, grupo)

    # Adicionar labels ao dataframe original.
    output_df = df.copy()
    output_df['cluster'] = clusters
    
    # salvar modelos
    preprocessing_pipe_path = f'{os.environ["ROOT_DIR"]}/src/models/preprocessing_pipe_{grupo}.joblib'
    cluster_path = f'{os.environ["ROOT_DIR"]}/src/models/cluster_{grupo}.joblib'
    
    dump(preprocessing_pipe, preprocessing_pipe_path) 
    dump(clustering_pipe, cluster_path)   

    return output_df


def round_(x):
    try:
        return round(x, 3)
    except:
        return x


def run(request_json):
    logging.debug("Iniciando pipeline de treino.")

    # ETL.
    df_perfil_por_empresa, df_perfil_por_plano, df_perfil_por_operadora = etl.run(request_json)

    # Treino.
    benchmark_empresas = training_pipeline(df_perfil_por_empresa)
    benchmark_planos = training_pipeline(df_perfil_por_plano, grupo='planos')
    benchmark_operadoras = training_pipeline(df_perfil_por_operadora, grupo='operadoras')
    
    # Salvar modelos GCP
    LOCAL_MODEL_FILEPATH = f'{os.environ["ROOT_DIR"]}/src/models/'
    all_files = []
    files_names = []
    for root, directories, files in os.walk(LOCAL_MODEL_FILEPATH):
        for name in files:
            file_name = os.path.join(root, name)
            all_files.append(file_name)
            files_names.append(name)
    
    for full_file_name, file in zip(all_files, files_names):
        destination_file = f'{os.environ["CLUSTER_FOLDER_NAME"]}/{file}'
        logging.debug(f'Salvando modelo {full_file_name} em {destination_file}')
        upload_file(
            bucket_name=os.environ['GCLOUD_BUCKET'],
            destination_blob_name=destination_file,
            source_file_path=full_file_name
        )
