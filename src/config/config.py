import os


class Config:
    def __init__(self):
        """Estas configurações valem para os ambientes de dev e prod."""
        os.environ['CONFIG_DIR'] = os.path.dirname(os.path.abspath(__file__))
        os.environ['SRC_DIR'] = os.path.dirname(os.getenv('CONFIG_DIR'))
        os.environ['ROOT_DIR'] = os.path.dirname(os.getenv('SRC_DIR'))

class ConfigDev(Config):
    def __init__(self):
        """Estas configurações valem apenas para o ambiente de dev."""
        super().__init__()
        os.environ['GCLOUD_PROJECT'] = 'laboratorio-eng-dados'
        os.environ['GCLOUD_CREDENTIALS_FILEPATH'] = os.path.join(os.environ['CONFIG_DIR'], 'gcloud_credentials_dev.json')
        os.environ['GCLOUD_BUCKET'] = 'dev-ml-model'
        os.environ['CLUSTER_FOLDER_NAME'] = 'prometheus_models'

class ConfigQa(Config):
    def __init__(self):
        """Estas configurações valem apenas para o ambiente de qa."""
        super().__init__()
        os.environ['GCLOUD_PROJECT'] = 'gesto-qa'
        os.environ['GCLOUD_CREDENTIALS_FILEPATH'] = os.path.join(os.environ['CONFIG_DIR'], 'machine-learning-qa-gesto-qa-dc1e6de3a282.json')
        os.environ['GCLOUD_BUCKET'] = 'qa-ml-model'
        os.environ['CLUSTER_FOLDER_NAME'] = 'prometheus_models'

class ConfigPrd(Config):
    def __init__(self):
        """Estas configurações valem apenas para o ambiente de prod."""
        super().__init__()
        os.environ['GCLOUD_PROJECT'] = 'cosmic-shift-235317'
        os.environ['GCLOUD_CREDENTIALS_FILEPATH'] = os.path.join(os.environ['CONFIG_DIR'], 'gcloud_credentials_prd.json')
        os.environ['GCLOUD_BUCKET'] = 'prd-ml-models'
        os.environ['CLUSTER_FOLDER_NAME'] = 'prometheus_models'
